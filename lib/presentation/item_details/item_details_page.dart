import 'package:flutter/material.dart';

class ItemDetailsPage extends StatelessWidget {
  const ItemDetailsPage({
    required this.itemId,
    super.key,
  });

  static String routeName = '/shop/:id';

  final int itemId;

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: const Text('Product Details'),
        ),
        body: ListView(
          padding: const EdgeInsets.all(16),
          children: [
            const AspectRatio(
              aspectRatio: 1.5,
              child: Placeholder(),
            ),
            const SizedBox(
              height: 16,
            ),
            Text(
              'Item #$itemId',
              style: const TextStyle(
                fontSize: 24,
              ),
            ),
            const SizedBox(
              height: 16,
            ),
            const Text('''
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque in ultricies turpis, vel posuere ipsum. Nam vel quam at lacus mattis accumsan. Ut sit amet velit porta, bibendum sapien in, tempus nulla. Quisque nibh enim, hendrerit in varius vel, volutpat ac augue. Cras at felis sit amet sem lacinia hendrerit ac vitae diam. Morbi aliquet lacus quis tristique finibus. Fusce in sapien aliquet, placerat tortor fermentum, varius ex. Curabitur ex arcu, congue sed malesuada a, sodales vitae sapien.

Praesent luctus ligula ac dolor tempus pretium. Donec at elit risus. Curabitur ac ante ultricies, fringilla lorem et, facilisis tortor. Curabitur sit amet interdum enim. Vivamus aliquet sodales mauris, non consectetur tortor lobortis nec. Ut in iaculis odio. In posuere a nunc vitae scelerisque. Nunc convallis ante sit amet ipsum volutpat ultricies.

Praesent nisl mauris, varius sed condimentum nec, laoreet eget turpis. Suspendisse a orci a mi sodales feugiat id sit amet justo. Nullam neque sem, tristique a pretium vitae, fringilla nec elit. Nulla facilisi. Nam pellentesque sed ligula nec rutrum. Praesent semper sapien in nibh sagittis ultrices. Donec fermentum convallis lacus, sed porta libero bibendum at.

Pellentesque eleifend, felis et facilisis commodo, tortor augue placerat eros, ac commodo metus leo nec nisi. Aliquam erat volutpat. Donec bibendum justo id laoreet tincidunt. Ut ultricies nulla a lacus aliquam feugiat. Donec volutpat, enim at ultrices accumsan, lorem neque porta tellus, sed posuere enim leo et sapien. Ut sed nunc luctus, bibendum tellus vitae, fermentum erat. Sed dui massa, laoreet sit amet dignissim eget, luctus a est. Nulla facilisi. Donec quis ligula consectetur, hendrerit tortor quis, tempus metus. In porta pulvinar turpis. Aliquam nec lacus diam.

In risus ante, condimentum ac erat ac, tincidunt imperdiet turpis. Nunc mattis in sapien in egestas. Morbi sagittis venenatis consequat. Proin sit amet varius neque, et sodales quam. Aliquam lacus lacus, finibus sit amet mi ut, porta mattis lectus. Nulla mollis arcu ut magna feugiat, in mollis nisl semper. Sed vulputate eget metus sed efficitur. Nullam erat erat, dictum sed commodo sed, iaculis eget est. Sed rhoncus elit felis. Aenean vitae ornare sem. Morbi sed viverra dolor, a interdum nulla.'''),
          ],
        ),
      );
}
