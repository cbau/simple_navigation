import 'package:flutter/material.dart';

import '../item_details/item_details_page.dart';

class ItemListPage extends StatelessWidget {
  const ItemListPage({super.key});

  static String routeName = '/shop';

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: const Text('Shop'),
        ),
        body: GridView.builder(
          padding: const EdgeInsets.all(16),
          gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
            crossAxisSpacing: 9,
            mainAxisExtent: 160,
            mainAxisSpacing: 8,
            maxCrossAxisExtent: 240,
          ),
          itemBuilder: (context, index) => Card(
            clipBehavior: Clip.antiAlias,
            child: InkWell(
              onTap: () async => Navigator.of(context).pushNamed(
                ItemDetailsPage.routeName.replaceAll(':id', '${index + 1}'),
              ),
              child: Column(
                children: [
                  const Expanded(
                    child: Placeholder(),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8),
                    child: Text('Item #${index + 1}'),
                  ),
                ],
              ),
            ),
          ),
        ),
      );
}
