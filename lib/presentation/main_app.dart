import 'package:flutter/material.dart';

import 'app_router.dart';
import 'item_list/item_list_page.dart';

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) => MaterialApp(
        darkTheme: ThemeData.dark(),
        initialRoute: ItemListPage.routeName,
        onGenerateRoute: AppRouter().onGenerateRoute,
        theme: ThemeData.light(),
        title: 'Simple Navigation',
      );
}
