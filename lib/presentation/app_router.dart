import 'package:flutter/material.dart';

import 'cart/cart_page.dart';
import 'item_details/item_details_page.dart';
import 'item_list/item_list_page.dart';
import 'navigation/navigation_scaffold.dart';
import 'profile/profile_page.dart';

class AppRouter {
  factory AppRouter() => _instance;

  AppRouter._();

  static final AppRouter _instance = AppRouter._();

  Route<Object?>? onGenerateRoute(RouteSettings settings) {
    final pathName = settings.name;

    if (pathName == CartPage.routeName) {
      return PageRouteBuilder(
        pageBuilder: (context, animation1, animation2) =>
            const NavigationScaffold(
          selectedIndex: 1,
          child: CartPage(),
        ),
        settings: settings,
        transitionDuration: Duration.zero,
        reverseTransitionDuration: Duration.zero,
      );
    }

    if (pathName?.startsWith(ItemDetailsPage.routeName.replaceAll(':id', '')) ??
        false) {
      final idValue = pathName!.split('/').last;
      final id = int.tryParse(idValue) ?? 0;
      return MaterialPageRoute(
        builder: (context) => ItemDetailsPage(
          itemId: id,
        ),
        settings: settings,
      );
    }

    if (pathName == ItemListPage.routeName) {
      return PageRouteBuilder(
        pageBuilder: (context, animation1, animation2) =>
            const NavigationScaffold(
          selectedIndex: 0,
          child: ItemListPage(),
        ),
        settings: settings,
        transitionDuration: Duration.zero,
        reverseTransitionDuration: Duration.zero,
      );
    }

    if (pathName == ProfilePage.routeName) {
      return PageRouteBuilder(
        pageBuilder: (context, animation1, animation2) =>
            const NavigationScaffold(
          selectedIndex: 2,
          child: ProfilePage(),
        ),
        settings: settings,
        transitionDuration: Duration.zero,
        reverseTransitionDuration: Duration.zero,
      );
    }

    return null;
  }
}
