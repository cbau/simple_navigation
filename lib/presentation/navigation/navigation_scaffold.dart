import 'package:flutter/material.dart';

import '../cart/cart_page.dart';
import '../item_list/item_list_page.dart';
import '../profile/profile_page.dart';

class NavigationScaffold extends StatelessWidget {
  const NavigationScaffold({
    required this.selectedIndex,
    this.child,
    super.key,
  });

  final Widget? child;
  final int selectedIndex;

  List<NavigationDestination> get _destinations => const [
        NavigationDestination(
          icon: Icon(Icons.shop_outlined),
          label: 'Shop',
          selectedIcon: Icon(Icons.shop),
        ),
        NavigationDestination(
          icon: Icon(Icons.shopping_cart_outlined),
          label: 'Cart',
          selectedIcon: Icon(Icons.shopping_cart),
        ),
        NavigationDestination(
          icon: Icon(Icons.person_outline),
          label: 'Profile',
          selectedIcon: Icon(Icons.person),
        ),
      ];

  @override
  Widget build(BuildContext context) {
    final isSmallScreen = MediaQuery.sizeOf(context).width < 600;

    return Scaffold(
      body: isSmallScreen
          ? child
          : Row(
              children: [
                NavigationRail(
                  destinations: _destinations
                      .map(
                        (destination) => NavigationRailDestination(
                          icon: destination.icon,
                          label: Text(destination.label),
                          selectedIcon: destination.selectedIcon,
                        ),
                      )
                      .toList(),
                  extended: true,
                  onDestinationSelected: (selectedIndex) async =>
                      _onDestinationSelected(context, selectedIndex),
                  selectedIndex: selectedIndex,
                ),
                if (child != null) Expanded(child: child!),
              ],
            ),
      bottomNavigationBar: isSmallScreen
          ? NavigationBar(
              destinations: _destinations,
              onDestinationSelected: (selectedIndex) async =>
                  _onDestinationSelected(context, selectedIndex),
              selectedIndex: selectedIndex,
            )
          : null,
    );
  }

  Future<void> _onDestinationSelected(
    BuildContext context,
    int selectedIndex,
  ) async {
    switch (selectedIndex) {
      case 1:
        await Navigator.of(context).pushReplacementNamed(
          CartPage.routeName,
        );
      case 2:
        await Navigator.of(context).pushReplacementNamed(
          ProfilePage.routeName,
        );
      default:
        await Navigator.of(context).pushReplacementNamed(
          ItemListPage.routeName,
        );
    }
  }
}
