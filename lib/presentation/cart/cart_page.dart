import 'package:flutter/material.dart';

class CartPage extends StatelessWidget {
  const CartPage({super.key});

  static String routeName = '/cart';

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: const Text('Cart'),
        ),
        body: const Padding(
          padding: EdgeInsets.all(16),
          child: Center(
            child: Text(
              "You haven't added any items to your cart yet.",
              style: TextStyle(
                color: Colors.grey,
              ),
            ),
          ),
        ),
      );
}
