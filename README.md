# Simple Navigation

Simple navigation usage with Navigation Bar on small screens and Navigation Rail on big screens.

## Features

- On small screens, a bottom navigation bar is used to navigate between  the main screens.
- On big screens, a navigation rail is used to navigate between the main screens.
- Navigation is done with `onGenerateRoute` from Flutter's standard Navigation 1.0.

## Releases

See this project working on web at this link:

[WebApp](https://cbau.gitlab.io/simple_navigation/)

## Screenshots

<img src="./screenshots/item-list-small.png" alt="Book list on small screens screenshot" width="240" /> <img src="./screenshots/item-list-big.png" alt="Book list on big screens screenshot" width="480" />

## Compile

To run this project on your device, run this command:

```bash
flutter run --release
```
